package mx.rcristian.adnagencia.utils;

import android.graphics.Color;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class Colors {

    public static final String RED = "#F34336";
    public static final String PINK = "#E81E63";
    public static final String PURPLE = "#9C27B0";
    public static final String DEEP_PURPLE = "#673AB7";
    public static final String INDIGO = "#3F51B5";
    public static final String BLUE = "#2096F2";
    public static final String LIGHT_BLUE = "#04A9F3";
    public static final String CYAN = "#01BCD3";
    public static final String TEAL = "#009588";
    public static final String GREEN = "#4CAF50";
    public static final String LIGHT_GREEN = "#8BC14A";
    public static final String LIME = "#CCDB3A";
    public static final String YELLOW = "#FEEA3B";
    public static final String AMBER = "#FDC009";
    public static final String ORANGE = "#FE9802";
    public static final String DEEP_ORANGE = "#FE5723";
    public static final String BROWN = "#795548";
    public static final String GRAY = "#9E9E9E";
    public static final String BLUE_GRAY = "#607D8B";

    public static int getColorRandom () {

        String[] colorList = {
                Colors.RED,
                Colors.PINK,
                Colors.PURPLE,
                Colors.DEEP_PURPLE,
                Colors.INDIGO,
                Colors.BLUE,
                Colors.LIGHT_BLUE,
                Colors.TEAL,
                Colors.GREEN,
                Colors.ORANGE,
                Colors.DEEP_ORANGE,
                Colors.BROWN,
                Colors.GRAY,
                Colors.BLUE_GRAY,
                Colors.CYAN,
            };

        Random rand = new Random();

        int indexColor = rand.nextInt( colorList.length - 1 );

        return Color.parseColor(colorList[indexColor]);
    }

}
