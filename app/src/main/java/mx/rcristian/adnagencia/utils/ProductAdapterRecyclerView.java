package mx.rcristian.adnagencia.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;

import mx.rcristian.adnagencia.R;
import mx.rcristian.adnagencia.models.Product;
import mx.rcristian.adnagencia.views.ProductActivity;

public class ProductAdapterRecyclerView extends RecyclerView.Adapter<ProductAdapterRecyclerView.ProductViewHolder> {

    private ArrayList<Product> productList;
    private int layout;
    private Activity activity;
    private FirebaseFirestore db;


    public ProductAdapterRecyclerView(ArrayList<Product> productList, int layout, Activity activity) {
        this.productList = productList;
        this.layout = layout;
        this.activity = activity;
        this.db = FirebaseFirestore.getInstance();
    }


    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductViewHolder holder, int position) {
        final Product product = productList.get(position);
        DecimalFormat df = new DecimalFormat("#.00");
        holder.cardView.setCardBackgroundColor( Colors.getColorRandom() );
        holder.name.setText( product.getName() );
        holder.code.setText( product.getCode() );
        holder.price.setText( "$ " + df.format(product.getUnitPrice()) );
        holder.more.setText( product.getUnit() + " / " + product.getPresentation() + " piezas / " + product.getDescription() );

        holder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(activity, holder.options);
                popup.getMenuInflater().inflate(R.menu.card_view_product, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.delete_product:
                                db.collection("stock").document(product.getId())
                                        .delete()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Snackbar.make( activity.findViewById(android.R.id.content), R.string.product_delete, Snackbar.LENGTH_LONG ).show();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Snackbar.make( activity.findViewById(android.R.id.content), R.string.failed_delete_product, Snackbar.LENGTH_LONG ).show();
                                            }
                                        });
                                return true;

                            case R.id.update_product:
                                Intent intent = new Intent(activity, ProductActivity.class);
                                intent.putExtra("id", product.getId());
                                activity.startActivity(intent);
                                return true;

                            default:
                                return true;
                        }

                    }
                });

                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private TextView name;
        private TextView code;
        private TextView price;
        private TextView more;
        private ImageView options;

        public ProductViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.product_card_view);
            name = (TextView) itemView.findViewById(R.id.name_product_card_view);
            code = (TextView) itemView.findViewById(R.id.code_product_card_view);
            price = (TextView) itemView.findViewById(R.id.price_product_card_view);
            more = (TextView) itemView.findViewById(R.id.more_card_view);
            options = (ImageView) itemView.findViewById(R.id.options);
        }

    }

}
