package mx.rcristian.adnagencia.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

import mx.rcristian.adnagencia.R;
import mx.rcristian.adnagencia.views.LoginActivity;


public class ProfileFragment extends Fragment {
    private Button signOut;
    private FirebaseAuth firebaseAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        signOut = (Button) view.findViewById(R.id.button_sign_out);

        firebaseAuth = FirebaseAuth.getInstance();

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle( getResources().getString(R.string.profile) );

        signOut.setOnClickListener(onClickListener);

        return view;
    }


    private View.OnClickListener onClickListener = new View.OnClickListener () {
        @Override
        public void onClick(View v) {
            firebaseAuth.signOut();
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity( intent );
            getActivity().finish();
        }
    };

}
