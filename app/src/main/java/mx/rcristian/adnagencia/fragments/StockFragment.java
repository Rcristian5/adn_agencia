package mx.rcristian.adnagencia.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import javax.annotation.Nullable;

import mx.rcristian.adnagencia.R;
import mx.rcristian.adnagencia.models.Product;
import mx.rcristian.adnagencia.utils.ProductAdapterRecyclerView;
import mx.rcristian.adnagencia.views.ProductActivity;


public class StockFragment extends Fragment {

    private Toolbar toolbar;
    private FirebaseFirestore db;
    private View view;
    RecyclerView productsRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_stock, container, false);
        setHasOptionsMenu(true);

        productsRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_products);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle( getResources().getString(R.string.stock) );

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        productsRecyclerView.setLayoutManager(linearLayoutManager);

        db = FirebaseFirestore.getInstance();
        db.collection("stock")
                .addSnapshotListener(snapshotListener);


        return view;
    }

    @Override
    public void onStart() {
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        super.onStart();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_stock, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.add_product:
                Intent intent = new Intent( getActivity(), ProductActivity.class );
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private EventListener<QuerySnapshot> snapshotListener = new EventListener<QuerySnapshot>() {
        @Override
        public void onEvent(@Nullable QuerySnapshot querySnapshot, @Nullable FirebaseFirestoreException e) {

            if (e != null) {
                Snackbar.make( view, getResources().getString(R.string.failed_request_product_list), Snackbar.LENGTH_LONG ).show();
                return;
            }

            ArrayList<Product> productList = new ArrayList<>();
            for (QueryDocumentSnapshot document : querySnapshot) {
                productList.add( document.toObject(Product.class) );
            }

            ProductAdapterRecyclerView productAdapterRecyclerView = new ProductAdapterRecyclerView( productList, R.layout.element_product_card_view, getActivity());
            productsRecyclerView.setAdapter(productAdapterRecyclerView);
        }
    };




}
