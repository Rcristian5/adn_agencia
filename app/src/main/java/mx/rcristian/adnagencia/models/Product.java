package mx.rcristian.adnagencia.models;

import com.google.firebase.firestore.PropertyName;

public class Product {

    @PropertyName("id")
    public String id;
    @PropertyName("name")
    public String name;
    @PropertyName("code")
    public String code;
    @PropertyName("unit")
    public String unit;
    @PropertyName("unitPrice")
    public Float unitPrice;
    @PropertyName("presentation")
    public String presentation;
    @PropertyName("description")
    public String description;
    @PropertyName("barcode")
    public String barcode;

    public Product() {
    }

    public Product(String name, String code, String unit, Float unit_price, String presentation, String description) {
        this.name = name;
        this.code = code;
        this.unit = unit;
        this.unitPrice= unit_price;
        this.presentation = presentation;
        this.description = description;
    }

    public Product(String name, String code, String unit, Float unit_price, String presentation, String description, String barcode) {
        this.name = name;
        this.code = code;
        this.unit = unit;
        this.unitPrice = unit_price;
        this.presentation = presentation;
        this.description = description;
        this.barcode = barcode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = this.id == null ? id : this.id ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
