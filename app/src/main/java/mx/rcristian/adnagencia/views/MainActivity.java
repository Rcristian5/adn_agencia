package mx.rcristian.adnagencia.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import mx.rcristian.adnagencia.R;
import mx.rcristian.adnagencia.fragments.HomeFragment;
import mx.rcristian.adnagencia.fragments.StockFragment;
import mx.rcristian.adnagencia.fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView navigationView;
    private final Fragment stockFragment = new StockFragment();
    private final Fragment homeFragment = new HomeFragment();
    private final Fragment profileFragment = new ProfileFragment();
    private final FragmentManager fm = getSupportFragmentManager();
    private Fragment fragmentActive = homeFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationView = (BottomNavigationView) findViewById(R.id.navigation);

        fm.beginTransaction().add(R.id.frame_container, stockFragment, "3").hide(stockFragment).commit();
        fm.beginTransaction().add(R.id.frame_container, profileFragment, "2").hide(profileFragment).commit();
        fm.beginTransaction().add(R.id.frame_container, homeFragment, "1").commit();
        homeFragment.onStop();
        profileFragment.onStop();
        stockFragment.onStop();

        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigationView.setSelectedItemId(R.id.navigation_home);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                
                case R.id.navigation_home:
                    fragmentActive.onStop();
                    homeFragment.onStart();
                    fm.beginTransaction().hide(fragmentActive).show(homeFragment).commit();
                    fragmentActive = homeFragment;
                    return true;

                case R.id.navigation_stock:
                    fragmentActive.onStop();
                    stockFragment.onStart();
                    fm.beginTransaction().hide(fragmentActive).show(stockFragment).commit();
                    fragmentActive = stockFragment;
                    return true;

                case R.id.navigation_profile:
                    fragmentActive.onStop();
                    profileFragment.onStart();
                    fm.beginTransaction().hide(fragmentActive).show(profileFragment).commit();
                    fragmentActive = profileFragment;
                    return true;

            }
            return true;
        }
    };


}
