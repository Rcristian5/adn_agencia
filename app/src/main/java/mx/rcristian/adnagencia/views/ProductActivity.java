package mx.rcristian.adnagencia.views;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.text.DecimalFormat;
import java.util.ArrayList;

import mx.rcristian.adnagencia.R;
import mx.rcristian.adnagencia.models.Product;

public class ProductActivity extends AppCompatActivity {

    private Button buttonCreateProduct;
    private ImageButton buttonBarcodeReader;
    private TextInputEditText textInputName;
    private TextInputEditText textInputCode;
    private MaterialBetterSpinner spinnerUnit;
    private TextInputEditText textInputUnitPrice;
    private TextInputEditText textInputPresentation;
    private TextInputEditText textInputDescription;
    private TextInputEditText textInputBarcode;
    private View content;
    private FirebaseFirestore db;
    private Product product;
    private boolean isViewCreated;
    private final int PICK_BARCODE_REQUEST = 31;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        content = (View) findViewById(android.R.id.content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        textInputName = (TextInputEditText) findViewById(R.id.text_input_name);
        textInputCode = (TextInputEditText) findViewById(R.id.text_input_code);
        spinnerUnit = (MaterialBetterSpinner) findViewById(R.id.spinner_unit);
        textInputUnitPrice = (TextInputEditText) findViewById(R.id.text_input_unit_price);
        textInputPresentation = (TextInputEditText) findViewById(R.id.text_input_presentation);
        textInputDescription = (TextInputEditText) findViewById(R.id.text_input_description);
        textInputBarcode = (TextInputEditText) findViewById(R.id.text_input_barcode);
        buttonCreateProduct = (Button) findViewById(R.id.button_create_product);
        buttonBarcodeReader = (ImageButton) findViewById(R.id.button_barcode_reader);

        spinnerUnit.setAdapter( buildOptionsSpinnerUnit() );
        db = FirebaseFirestore.getInstance();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String idProduct = getIntent().getStringExtra("id");
        if( idProduct == null ) {
            isViewCreated = true;
            getSupportActionBar().setTitle( getResources().getString(R.string.new_product) );
        }
        else {
            isViewCreated = false;
            buttonCreateProduct.setText(getResources().getString(R.string.update_product));
            db.collection("stock").document(idProduct)
                    .get()
                    .addOnCompleteListener(onCompleteListener);
        }


        buttonCreateProduct.setOnClickListener(onClickListener);
        buttonBarcodeReader.setOnClickListener(onClickListener);
    }


    private ArrayAdapter<String> buildOptionsSpinnerUnit () {
        String[] options = { getResources().getString(R.string.box) , getResources().getString(R.string.piece)};
        return new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, options);
    }


    private Product buildProduct () {

        String name = textInputName.getText().toString().trim();
        String code = textInputCode.getText().toString().trim();
        String unit = spinnerUnit.getText().toString().trim();
        String unitPrice = textInputUnitPrice.getText().toString().trim();
        String presentation = textInputPresentation.getText().toString().trim();
        String description = textInputDescription.getText().toString().trim();
        String barcode = textInputBarcode.getText().toString().trim();

        if( name.isEmpty() || code.isEmpty() || unit.isEmpty() || unitPrice.isEmpty() || presentation.isEmpty() || description.isEmpty() ) {
            return null;
        }

        return new Product(name, code, unit, Float.parseFloat(unitPrice), presentation, description, barcode);
    };


    private Product updateProduct ( Product product ) {

        String name = textInputName.getText().toString().trim();
        String code = textInputCode.getText().toString().trim();
        String unit = spinnerUnit.getText().toString().trim();
        String unitPrice = textInputUnitPrice.getText().toString().trim();
        String presentation = textInputPresentation.getText().toString().trim();
        String description = textInputDescription.getText().toString().trim();
        String barcode = textInputBarcode.getText().toString().trim();

        if( name.isEmpty() || code.isEmpty() || unit.isEmpty() || unitPrice.isEmpty() || presentation.isEmpty() || description.isEmpty() ) {
            return null;
        }

        product.setName(name);;
        product.setCode(code);
        product.setUnit(unit);
        product.setUnitPrice( Float.parseFloat(unitPrice) );
        product.setPresentation(presentation);
        product.setDescription(description);
        product.setBarcode(barcode);

        return product;
    };


    private void emptyFieldProduct () {
        textInputName.setText("");
        textInputCode.setText("");
        spinnerUnit.setText("");
        textInputUnitPrice.setText("");
        textInputPresentation.setText("");
        textInputDescription.setText("");
        textInputBarcode.setText("");
    }


    private void setFieldProduct ( Product product ) {
        textInputName.setText( product.getName() );
        textInputCode.setText(  product.getCode() );
        spinnerUnit.setText( product.getUnit() );
        textInputUnitPrice.setText( product.getUnitPrice().toString() );
        textInputPresentation.setText( product.getPresentation() );
        textInputDescription.setText(  product.getDescription() );
        textInputBarcode.setText( product.getBarcode() );
    }


    private void enableFieldProduct( boolean enable ) {
        textInputName.setEnabled(enable);
        textInputCode.setEnabled(enable);
        spinnerUnit.setEnabled(enable);
        textInputUnitPrice.setEnabled(enable);
        textInputPresentation.setEnabled(enable);
        textInputDescription.setEnabled(enable);
        textInputBarcode.setEnabled(enable);
    }


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button_create_product:
                    onClickCreatProduct();
                    return;

                case R.id.button_barcode_reader:
                    onClickBarcodeReader();
                    return;
            }

        }
    };


    private void onClickBarcodeReader () {
        Intent intent = new Intent(this, BarcodeReaderActivity.class);
        startActivityForResult(intent, PICK_BARCODE_REQUEST);
    };


    private void onClickCreatProduct () {
        if( product != null ) {

            product = updateProduct( product );

            db.collection("stock").document(product.getId())
                    .set(product)
                    .addOnSuccessListener(onSuccessListenerSaveProduct)
                    .addOnFailureListener(onFailureListenerSaveProduct);
        }
        else {
            product = buildProduct();

            if (product != null) {
                buttonCreateProduct.setEnabled(false);
                enableFieldProduct(false);

                DocumentReference docRef = db.collection("stock").document();
                product.setId(docRef.getId());

                docRef
                        .set(product)
                        .addOnSuccessListener(onSuccessListenerSaveProduct)
                        .addOnFailureListener(onFailureListenerSaveProduct);
            } else {
                Snackbar.make(content, R.string.fields_empty_product, Snackbar.LENGTH_LONG).show();
            }
        }
    }


    private OnCompleteListener<DocumentSnapshot> onCompleteListener  = new OnCompleteListener<DocumentSnapshot>() {
        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {

            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    product = document.toObject(Product.class);
                    getSupportActionBar().setTitle(product.getName());
                    setFieldProduct(product);
                } else {
                    Snackbar.make( content, getResources().getString(R.string.failed_product_empty), Snackbar.LENGTH_LONG ).show();
                }
            } else {
                Snackbar.make( content, getResources().getString(R.string.failed_get_product), Snackbar.LENGTH_LONG ).show();
            }
        }
    };



    private OnSuccessListener<Void> onSuccessListenerSaveProduct = new OnSuccessListener<Void>() {

        @Override
        public void onSuccess(Void aVoid) {
            buttonCreateProduct.setEnabled(true);
            emptyFieldProduct();
            enableFieldProduct(true);
            Snackbar.make( content, getResources().getString(R.string.product_saved), Snackbar.LENGTH_LONG ).show();

            if( !isViewCreated ) {
                finish();
            }
        }
    };


    private OnFailureListener onFailureListenerSaveProduct = new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
            buttonCreateProduct.setEnabled(true);
            enableFieldProduct(true);
            Snackbar.make( content, getResources().getString(R.string.failed_product_saved), Snackbar.LENGTH_LONG ).show();
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( requestCode == PICK_BARCODE_REQUEST ) {
            if ( resultCode == RESULT_OK ) {
                String barcode = data.getStringExtra("barcode");
                textInputBarcode.setText(barcode);
            }
        }
    }
}
