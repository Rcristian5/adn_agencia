package mx.rcristian.adnagencia.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import mx.rcristian.adnagencia.R;

public class BarcodeReaderActivity extends AppCompatActivity {

    private CameraSource cameraSource;
    private SurfaceView cameraView;
    private TextView tvBarcode;
    private FloatingActionButton fab_check_barcode;
    private View content;
    private final int PERMISSIONS_REQUEST_CAMERA = 31;
    private String barcode_read;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_reader);

        cameraView = (SurfaceView) findViewById(R.id.camera_view);
        tvBarcode = (TextView) findViewById(R.id.text_view_barcode_info);
        fab_check_barcode = (FloatingActionButton) findViewById(R.id.fab_check_barcode);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        appBarLayout.bringToFront();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);

        content = (View) findViewById(android.R.id.content);
        fab_check_barcode.setOnClickListener(onClickListenerCheckBarcode);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                Snackbar.make(content, R.string.explanation_camera, Snackbar.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            }
        } else {
            initCameraScanner();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initCameraScanner();
                } else {
                    finish();
                }
                break;

        }
    }


    private void initCameraScanner() {
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this).build();
        cameraSource = new CameraSource.Builder(this, barcodeDetector).setAutoFocusEnabled(true).build();
        cameraView.setVisibility(View.VISIBLE);
        cameraView.getHolder().addCallback(surfaceHolderCallback);
        barcodeDetector.setProcessor(detectorProcesor);
    }


    private SurfaceHolder.Callback surfaceHolderCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                if (ActivityCompat.checkSelfPermission(BarcodeReaderActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                cameraSource.start(cameraView.getHolder());
            } catch (IOException e) {
                Snackbar.make(content, R.string.failed_open_camera, Snackbar.LENGTH_LONG).show();
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            cameraSource.stop();
        }
    };


    private Detector.Processor<Barcode> detectorProcesor = new Detector.Processor<Barcode>() {
        @Override
        public void release() {

        }

        @Override
        public void receiveDetections(Detector.Detections<Barcode> detections) {
            final SparseArray<Barcode> barcodes = detections.getDetectedItems();

            if (barcodes.size() != 0) {
                tvBarcode.post(new Runnable() {
                    @Override
                    public void run() {
                        cameraSource.stop();
                        barcode_read = barcodes.valueAt(0).displayValue;
                        tvBarcode.setText( barcode_read );
                        tvBarcode.setVisibility(View.VISIBLE);
                        fab_check_barcode.setVisibility(View.VISIBLE);
                    }
                });
            }
        }
    };


    private View.OnClickListener onClickListenerCheckBarcode = new View.OnClickListener () {

        @Override
        public void onClick(View v) {
            Intent intent = getIntent();
            intent.putExtra("barcode", barcode_read );
            setResult( RESULT_OK, intent );
            finish();
        }
    };

}