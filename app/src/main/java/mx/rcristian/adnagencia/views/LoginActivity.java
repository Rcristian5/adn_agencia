package mx.rcristian.adnagencia.views;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import mx.rcristian.adnagencia.R;

public class LoginActivity extends AppCompatActivity {

    private Button buttonLogin;
    private TextInputEditText textInputUsername;
    private TextInputEditText textInputPassword;
    private View content;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        buttonLogin = (Button) findViewById(R.id.button_sign_in);
        textInputUsername = (TextInputEditText) findViewById(R.id.text_input_username);
        textInputPassword = (TextInputEditText) findViewById(R.id.text_input_password);
        content = (View) findViewById(android.R.id.content);
        firebaseAuth = FirebaseAuth.getInstance();

        buttonLogin.setOnClickListener(onClickListener);
    }


    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        if( currentUser != null ) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }
    }


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String username = textInputUsername.getText().toString().trim();
            String password = textInputPassword.getText().toString().trim();


            if( username.length() == 0 || password.length() == 0) {
                Snackbar.make( content, R.string.email_or_password_empty, Snackbar.LENGTH_LONG ).show();
            }
            else {
                firebaseAuth
                        .signInWithEmailAndPassword(username, password)
                        .addOnCompleteListener( LoginActivity.this, onAuthentication );
            }
        }
    };


    private OnCompleteListener<AuthResult> onAuthentication = new OnCompleteListener<AuthResult>() {
        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if ( task.isSuccessful() ){
                startActivity( new Intent(LoginActivity.this, MainActivity.class) );
            }
            else {
                Snackbar.make( content, R.string.email_or_password_incorrect, Snackbar.LENGTH_LONG ).show();
            }
        }
    };
}
